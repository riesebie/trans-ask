## trans-ask ##

Request translation at a dictionary via browser. ATM LEO, DeepL, PONS
and Google Translator are available.

This is a small shell script which runs on POSIX shells.
Language combinations:

```
Spanish <-> German
English <-> German
Spanish <-> English
```

## Installation: ##

Don't install as root! Just `mkdir $HOME/tmp`, `cd $HOME/tmp`, clone
this repo and run `install.sh` from src dir. To work properly, `yad`
has to be installed. This is done by `install.sh` for OpenSUSE, Fedora,
FreeBSD and Debian derivats. `yad` should be avaialble on any other
Linux-Distro or \*BSD version. So either do it by youself or ask your
sysadmin ;-)

`trans-ask.sh` will be installed in `$HOME/bin`. You'll get starters
in `applications submenu education` and on the desktop.  To uninstall
trans-ask run `uninstall.sh` from src dir. 

If you want to start the script from commandline you have to

```
echo "export PATH=$PATH:$HOME/bin" >> $HOME/.profile
```

or any other shellrc like `zshrc`

## Main author ##

Send any other comments, patches, sweets and suggestions to:

[Elimar Riesebieter](mailto:riesebie@lxtec.de)



