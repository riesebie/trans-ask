#!/bin/sh
rm $HOME/bin/trans-ask.sh \
   $HOME/.local/share/applications/trans-ask.desktop
xdg-icon-resource uninstall --size 48 accessories-dictionary
xdg-desktop-icon uninstall trans-ask.desktop
