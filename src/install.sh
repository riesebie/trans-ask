#!/bin/sh
#set -x

# To install yad automatically a routine to test distribution specific
# installation commands is needed. Those are too much. User should know 
# how to install packages or even ask the responsible sys admin to do it.
# zypper install yad
# dnf install yad
# FIXME: pacman -S yad collides with /usr/games/pacman!
# apt install yad
# pkg install yad
# ...

if  ! [ -x "/usr/bin/yad" -o -x "/usr/local/bin/yad" ]
  then
    echo ""
    echo "yad needs to be installed.\n Requires sudo password!"
    echo ""
    if [ -x "$(which zypper)" 2>/dev/null ]
      then
        sudo zypper install yad
    elif [ -x "$(which dnf)" 2>/dev/null ]
      then
        sudo dnf install yad
    elif [ -x "$(which apt)" 2>/dev/null ]
      then
        sudo apt install yad
    elif [ -x "$(which pkg)" 2>/dev/null ]
      then
        sudo pkg install yad
    else
      echo "Don't know how to install yad"
      exit
    fi
fi

if ! [ -d "$HOME/.local/share/applications" ]
  then
    mkdir -p  $HOME/.local/share/applications
fi

if ! [ -d "$HOME/bin" ]
  then
    mkdir -p  $HOME/bin
fi

# FIXME: Not needed when running trans-ask.sh from dektop file.
# echo export $PATH=$PATH:$HOME/bin >> $HOME/.profile

cp trans-ask.sh $HOME/bin
chmod 755 $HOME/bin/trans-ask.sh

EXEC=$(echo $HOME/bin/trans-ask.sh)

cat<< EOF > $HOME/.local/share/applications/trans-ask.desktop
[Desktop Entry]
Version=1.0
Type=Application
Name=Trans-Ask
Comment=Translation request by browser
Exec=sh -c $EXEC
Icon=accessories-dictionary
Categories=Education;Dictionary
Terminal=false
StartupNotify=true
EOF

xdg-icon-resource install --size 48 accessories-dictionary.png accessories-dictionary

xdg-desktop-icon install $HOME/.local/share/applications/trans-ask.desktop
