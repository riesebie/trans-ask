###############################################################################
# Description:   Ask for a translation via firefox or chromium
# Author:        Elimar Riesebieter <riesebie@lxtec.de>
# Created:       Sun, 12 Jul 2020 09:14:20 +0200
# Last modified: Thu, 30 Jul 2020 19:30:00 +0200
# File:          $HOME/bin/trans-ask.sh
# License:       GPL2 or any later Version
# Copyright:     Copyright (C) 2020 Elimar Riesebieter <riesebie@lxtec.de>
# Version:       0.1 initial release
#                0.2 added browser select (firefox <-> chromium)
#                    added install-sh and uninstall.sh
#                    use of English language in script output
#                0.3 Added sh -c to Exec parameter in .desktop file
#                0.4 Doing some cosmetics
###############################################################################
#!/bin/sh
# set -x

ST=$(yad --title="Ask for translation" \
  --geometry 380x260 \
  --center \
  --on-top \
  --text="Choose languages" \
  --list --column=Check:RD \
  --column="Input language" \
  --column="Output language" \
  false English German \
  false Spanish German \
  false German English \
  false German Spanish \
  false English Spanish \
  false Spanish English \
  --button="Cancel":1 --button="Start":0) 2>/dev/null

ec=$(echo $?)
if [ $ec -eq 1 ]; then
  exit
elif [ $ec -eq 252 ]; then
  exit
fi

IPL=$(echo $ST | cut -d \| -f2)
OPL=$(echo $ST | cut -d \| -f3)
VOC=$(yad --title="Ask for translation" \
  --center \
  --on-top \
  --text="Fill in search word" \
  --form \
  --separator="" \
  --field="Key in or paste saerch word:CBE" Text \
  --button="Cancel":1 --button="Start":0) 2>/dev/null

ec=$(echo $?)
if [ $ec -eq 1 ]; then
  exit
elif [ $ec -eq 252 ]; then
  exit
fi

yad --title="Ask for translation" \
  --center \
  --on-top \
  --info --text="Entries:\n $IPL $OPL $VOC" 2>/dev/null

if test "$IPL" = 'English' && test "$OPL" = 'German'
   then
      LEO="dict.leo.org/englisch-deutsch/$VOC"
      DEEPL="www.deepl.com/translator#en/de/$VOC"
      PONS="https://de.pons.com/übersetzung/englisch-deutsch/$VOC"
      GOOGLE="translate.google.com/?hl=de#view=home&op=translate&sl=en&tl=de&text=$VOC"
elif test "$IPL" = 'Spanish' && test "$OPL" = 'German'
   then
      LEO="dict.leo.org/spanisch-deutsch/$VOC"
      DEEPL="www.deepl.com/translator#es/de/$VOC"
      PONS="https://de.pons.com/übersetzung/spanisch-deutsch/$VOC"
      GOOGLE="translate.google.com/?hl=de#view=home&op=translate&sl=es&tl=de&text=$VOC"
elif test "$IPL" = 'German' && test "$OPL" = 'English'
     then
        LEO="dict.leo.org/englisch-deutsch/$VOC\?side=right"
        DEEPL="www.deepl.com/translator#de/en/$VOC"
        PONS="https://de.pons.com/übersetzung/deutsch-englisch/$VOC"
        GOOGLE="translate.google.com/?hl=de#view=home&op=translate&sl=de&tl=en&text=$VOC"
elif test "$IPL" = 'German' && test "$OPL" = 'Spanish'
   then
      LEO="dict.leo.org/spanisch-deutsch/$VOC\?side=right"
      DEEPL="www.deepl.com/translator#de/es/$VOC"
      PONS="https://de.pons.com/übersetzung/deutsch-spanisch/$VOC"
      GOOGLE="translate.google.com/?hl=de#view=home&op=translate&sl=de&tl=es&text=$VOC"
elif test "$IPL" = 'English' && test "$OPL" = 'Spanish'
   then
      LEO="dict.leo.org/spanish-english/$VOC\?side=left"
      DEEPL="www.deepl.com/translator#en/es/$VOC"
      PONS="https://de.pons.com/übersetzung/englisch-spanisch/$VOC"
      GOOGLE="translate.google.com/?hl=de#view=home&op=translate&sl=en&tl=es&text=$VOC"
elif test "$IPL" = 'Spanish' && test "$OPL" = 'English'
   then
      LEO="dict.leo.org/spanish-english/$VOC\?side=right"
      DEEPL="www.deepl.com/translator#es/en/$VOC"
      PONS="https://de.pons.com/übersetzung/spanisch-englisch/$VOC"
      GOOGLE="translate.google.com/?hl=de#view=home&op=translate&sl=es&tl=en&text=$VOC"
fi

if [ -x "$(which firefox)" 2>/dev/null ]
   then
   FF="Firefox"
   else
   FF="Firefox is not installed"
fi
if [ -x "$(which chromium)" 2>/dev/null ]
   then
   CHR="Chromium"
   else
   CHR="Chromium is not installed"
fi

BR=$(yad --title="Choose Browser" \
  --geometry 380x260 \
  --center \
  --on-top \
  --text="Select Browser" \
  --list --column=Check:RD \
  --column=Browser \
  false "$FF" \
  false "$CHR" \
  --button="Cancel":1 --button="Start":0) 2>/dev/null

ec=$(echo $?)
if [ $ec -eq 1 ]; then
  exit
elif [ $ec -eq 252 ]; then
  exit
fi


BR=$(echo $BR | cut -d \| -f"2 4 6" )

if test "$BR" = 'Firefox'
   then
     BR="$(which firefox)"
     BRS="Firefox"
elif test "$BR" = "Chromium"
   then
     BR="$(which chromium)"
     BRS="Chromium"
fi

if test "$BRS" = 'Firefox' || test "$BRS" = 'Chromium'
   then
      nohup $BR $LEO $DEEPL $PONS $GOOGLE & >>/dev/null 2>&1
      yad --title="Ask for translation" \
         --width=300 \
         --no-buttons \
         --center \
         --on-top \
         --timeout=5 \
         --timeout-indicator=top \
         --text-align=center \
         --text="<b><big><big>$BRS is fired up</big></big></b>" 2>/dev/null
   else
        yad --title="Ask for translation" \
        --center \
        --on-top \
        --info --text="Wrong browser selected!" 2>/dev/null
fi
# vim:ft=sh:tw=0:et
